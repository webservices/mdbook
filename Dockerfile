FROM cern/cc7-base

ARG MDBOOK_VERSION="0.2.1"
LABEL version=$MDBOOK_VERSION

RUN yum install -y cargo && \
    cargo install mdbook --vers ${MDBOOK_VERSION} && \
    ln -s /root/.cargo/bin/mdbook /usr/local/bin/

